#!/bin/sh
pushd images
for file in *; do
    # ファイル名を取り出す
    fname="${file%.*}"
    convert ${file} ../pdf/${fname}.pdf 
done
popd
: