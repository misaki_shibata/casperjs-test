範囲指定してPDFの形で自動で吐き出す

コマンド1 (範囲指定してキャプチャ ChasperJS)
```
$ casperjs index.js <URL> <セレクタ> <ファイル名>

例) casperjs index.js http://yahoo.co.jp #cmprikunabi test.png
```

コマンド1 (連続実行)
```
$ casperjs loopCap.js 

取得設定はloopCap.js内に記述
```

コマンド2 (png/*.png →pdf/*.pdf ImageMagic)
```
$ sh png2pdf.sh 
```


