// var fs = require('fs'); // casperの場合は nodeとは別のfs moduleになる。　詳しくはhttp://wiki.commonjs.org/wiki/Filesystem/A#Files
// var files = fs.read('filepath.json');

// 何故か同じURLは2回処理出来ない。
var files = [
{"url":"http://yahoo.co.jp","selector":"#cmprikunabi","filename":"text1.png"},
{"url":"http://auctions.yahoo.co.jp/","selector":"#masthead","filename":"text2.png"}
];

var casper = require('casper').create({
    verbose: true,
    logLevel: "debug"
});
casper.start().each(files, function(self, obj) {
    console.log(obj);
    self.thenOpen(obj.url, function() {
        this.captureSelector('images/'+ obj.filename, obj.selector);
    });
});
casper.userAgent("Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26");
casper.viewport(1024, 768);

casper.run();

